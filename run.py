import logging
from enum import IntEnum, Enum
from typing import List, Tuple, Dict, Optional


class CardinalityNotFound(Exception):
    pass


class LoadSchemaError(Exception):
    pass


class SchemaMap(IntEnum):
    ATTRIBUTE = 0
    CARDINALITY = 2


class FactMap(IntEnum):
    NAME = 0
    TYPE = 1
    VALUE = 2
    IS_RETRACTION = 3


class Cardinality(Enum):
    ONE = 'one'
    MANY = 'many'


def load_schema(schema: List[Tuple]) -> Dict:
    """
    Transformando a estrutura do schema em um dict
    """
    try:
        return {sch[SchemaMap.ATTRIBUTE.value]: sch[SchemaMap.CARDINALITY.value]
            for sch in schema}
    except IndexError:
        msg = 'Quantidade de itens no schema enviado deve ser 3'
        logging.error(msg)
        raise LoadSchemaError(msg)

def create_key(fact: Tuple, schema: Dict, fact_type: str):
    """
    Criando identificação única para registro, levando em consideração a
    sua cardinalidade
    """
    def _array_limit(limit: int):
        return fact[FactMap.NAME.value:limit + 1]

    if schema[fact_type] == Cardinality.ONE.value:
        return ':'.join(_array_limit(limit=FactMap.TYPE.value))
    elif schema[fact_type] == Cardinality.MANY.value:
        return ':'.join(_array_limit(limit=FactMap.VALUE.value))
    else:
        msg = 'Cardinalidade fora do padrão requirido'
        logging.error(msg)
        raise CardinalityNotFound(msg)


def main(facts: List[Tuple], schema: List[Tuple]) -> Optional[List[Tuple]]:
    """
    Função principal que retornará o resultado esperado seguindo as regras
    pedidas.
    """
    try:
        schema = load_schema(schema)
    except LoadSchemaError:
        return

    results = {}
    for fact in facts:
        if len(fact) != 4:
            logging.error('Quantidade de item na tupla deve ser 4.')
            return

        fact_type = fact[FactMap.TYPE.value]
        try:
            key = create_key(fact, schema, fact_type)
        except CardinalityNotFound:
            break

        # Retraction rule
        if not fact[FactMap.IS_RETRACTION.value]:
            results.pop(key, None)
            continue

        results.update({key: fact})

    return list(results.values())
