Resolução
---

O desafio foi feito utilizando python3.7

Criei um arquivo `playground.py` para que o avaliador caso deseje executar o
exemplo do gist ou até outros cenários utilize-o.

    python playground.py

Executando os testes unitários

    python tests.py

