import unittest
import logging

from run import main, create_key, CardinalityNotFound, load_schema, \
    LoadSchemaError

logger = logging.getLogger()
logger.disabled = True


class MainTests(unittest.TestCase):
    def setUp(self) -> None:
        self.schema = [
            ('endereço', 'cardinality', 'one'),
            ('telefone', 'cardinality', 'many')
        ]

    def test_success_case(self):
        facts = [
            ('gabriel', 'endereço', 'av rio branco, 109', True),
            ('joão', 'endereço', 'rua alice, 10', True),
            ('joão', 'endereço', 'rua bob, 88', True),
            ('joão', 'telefone', '234-5678', True),
            ('joão', 'telefone', '91234-5555', True),
            ('joão', 'telefone', '234-5678', False),
            ('gabriel', 'telefone', '98888-1111', True),
            ('gabriel', 'telefone', '56789-1010', True),
        ]

        result = main(facts, self.schema)
        expected = [
            ('gabriel', 'endereço', 'av rio branco, 109', True),
            ('joão', 'endereço', 'rua bob, 88', True),
            ('joão', 'telefone', '91234-5555', True),
            ('gabriel', 'telefone', '98888-1111', True),
            ('gabriel', 'telefone', '56789-1010', True),
        ]

        self.assertListEqual(result, expected)

    def test_case_when_has_retraction(self):
        facts = [
            ('joão', 'telefone', '234-5678', True),
            ('joão', 'telefone', '91234-5555', True),
            ('joão', 'telefone', '234-5678', False),
        ]

        result = main(facts, self.schema)
        expected = [
            ('joão', 'telefone', '91234-5555', True),
        ]

        self.assertListEqual(result, expected)

    def test_case_when_cardinality_is_one(self):
        facts = [
            ('joão', 'endereço', 'rua alice, 10', True),
            ('joão', 'endereço', 'rua bob, 88', True)
        ]

        result = main(facts, self.schema)
        expected = [
            ('joão', 'endereço', 'rua bob, 88', True)
        ]

        self.assertListEqual(result, expected)

    def test_case_when_cardinality_is_many(self):
        facts = [
            ('joão', 'telefone', '234-5678', True),
            ('joão', 'telefone', '91234-5555', True),
            ('joão', 'telefone', '8234-5678', True)
        ]

        result = main(facts, self.schema)

        self.assertListEqual(result, facts)

    def test_case_including_new_item_in_schema(self):
        schema = [
            ('endereço', 'cardinality', 'one'),
            ('telefone', 'cardinality', 'many'),
            ('nome_mae', 'cardinality', 'one'),
            ('site', 'cardinality', 'many')
        ]

        facts = [
            ('gabriel', 'endereço', 'av rio branco, 109', True),
            ('joão', 'nome_mae', 'Maria', True),
            ('joão', 'nome_mae', 'Maria da Silva', True),
            ('joão', 'site', 'www.site.com', True),
            ('joão', 'site', 'www.twitter.com', True),
            ('joão', 'telefone', '234-5678', True),
            ('gabriel', 'telefone', '98888-1111', True),
            ('gabriel', 'telefone', '56789-1010', True),
        ]

        result = main(facts, schema)
        expected = [
            ('gabriel', 'endereço', 'av rio branco, 109', True),
            ('joão', 'nome_mae', 'Maria da Silva', True),
            ('joão', 'site', 'www.site.com', True),
            ('joão', 'site', 'www.twitter.com', True),
            ('joão', 'telefone', '234-5678', True),
            ('gabriel', 'telefone', '98888-1111', True),
            ('gabriel', 'telefone', '56789-1010', True)
        ]

        self.assertListEqual(result, expected)

    def test_case_when_index_in_facts_not_exist(self):
        facts = [
            ('gabriel', 'endereço', True)
        ]

        result = main(facts, self.schema)
        self.assertEqual(result, None)

    def test_case_when_retraction_not_exist(self):
        facts = [
            ('joão', 'telefone', '234-5678', True),
            ('joão', 'telefone', '91234-5555', True),
            ('joão', 'telefone', '7234-5678', False),
        ]

        result = main(facts, self.schema)
        expected = [
            ('joão', 'telefone', '234-5678', True),
            ('joão', 'telefone', '91234-5555', True)
        ]

        self.assertListEqual(result, expected)


class CreateKeyTests(unittest.TestCase):
    def setUp(self) -> None:
        self.schema = {'endereço': 'one', 'telefone': 'many'}

    def test_case_cardinality_one(self):
        fact = ('gabriel', 'endereço', 'av rio branco, 109', True)
        result = create_key(fact, self.schema, fact_type='endereço')
        self.assertEqual(result, 'gabriel:endereço')

    def test_case_cardinality_many(self):
        fact = ('joão', 'telefone', '234-5678', True)
        result = create_key(fact, self.schema, fact_type='telefone')
        self.assertEqual(result, 'joão:telefone:234-5678')

    def test_case_cardinality_unmapped(self):
        self.schema.update({'e-mail': 'nao_existo'})
        fact = ('joão', 'e-mail', '234-5678', True)
        with self.assertRaises(CardinalityNotFound):
            create_key(fact, self.schema, fact_type='e-mail')


class LoadSchemaTests(unittest.TestCase):
    def test_case_success(self):
        schema = [
            ('endereço', 'cardinality', 'one'),
            ('telefone', 'cardinality', 'many')
        ]
        expected = {'endereço': 'one', 'telefone': 'many'}
        result = load_schema(schema)
        self.assertEqual(result, expected)

    def test_case_when_qty_items_in_tuple_is_incorrect(self):
        schema = [
            ('endereço', 'one'),
            ('telefone', 'many')
        ]
        with self.assertRaises(LoadSchemaError):
            load_schema(schema)

if __name__ == '__main__':
    unittest.main()
